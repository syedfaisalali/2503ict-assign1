<!-- display results -->
<!DOCTYPE html>
<!-- Results page of associative array search example. -->
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/2503ict-assign1/socialNetwork/public/css/bootstrap.css">
</head>

<body>
  <div class="container">
    <div class="row navbar"  style="border: 2px solid black; background-color:#E6E6E6;">
      <div class="navbar-inner">
        <ul class="nav col-sm-6 col-xs-12">
          <li><a href="{{ secure_url('/') }}">Social Network</a></li>
        </ul>
        <ul class="nav col-sm-2 col-xs-12">
        </ul>
        <ul class="nav col-sm-2 col-xs-12">
          <li><a href="{{ secure_url('/friends') }}">Friends</a></li>
        </ul>
        <ul class="nav col-sm-2 col-xs-12">
          <li><a href="{{ secure_url('/documents') }}">Documents</a></li>
        </ul>
      </div>
    </div>
    @section('content')
    @show
  </div>
</body>
</html>