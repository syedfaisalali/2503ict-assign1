@extends('layouts.master')

@section('title')
Social Network - Friends
@stop

@section('content')
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div>
            <h3>Name:</h3>
            <span>Syed Faisal Ali</span>
        </div>
        <div>
            <h3>Message:</h3>
             <textarea rows="3" cols="30">
             </textarea> 
        </div>
        <input type="button" value="Post" class="offset2">
    </div>
    
    <div class="col-sm-8 col-xs-12">
        <?php
            $noOfFriends = count($friends);
            for ($i = 0; $i < $noOfFriends; $i++) {
                echo    '<div class="row" style="border: 2px solid black; margin:5px">';
                echo    '<div class="col-sm-4 col-xs-4">';
                echo    '<img src="'.$friends[$i]['image'].'" alt="no image" height="60" width="60" />';
                echo    '</div>';
                echo    '    <div class="col-sm-4 col-xs-7">';
                echo    '        <h5>'.$friends[$i]['name'].'</h5>';
                echo    '        <div>'.$friends[$i]['email'].'</div>';
                echo    '    </div>';
                echo    '</div>';
            };
        ?>
    </div>
    
</div>
@stop
