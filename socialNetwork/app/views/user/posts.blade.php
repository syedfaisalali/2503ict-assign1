@extends('layouts.master')

@section('title')
Social Network - Posts
@stop

@section('content')
    <div class="row">
        <div class="col-sm-4 col-xs-12">
            <h3>Make a Post</h3>
            <form action="add_post_action" method="post">
                <div>
                    <h4>Name:</h4>
                    <span><input type="text" name="username" style="width:250px;" required /></span>
                </div>
                <div>
                    <h4>Title:</h4>
                    <span><input type="text" name="title" style="width:250px;" required/></span>
                </div>
                <div>
                    <h4>Message:</h4>
                     <textarea name="message" style="width:250px; height:80px; required"></textarea> 
                </div>
                <input type="submit" value="Post" class="offset2">
            </form>
            
        </div>
        
        <div class="col-sm-8 col-xs-12">
            @foreach($posts as $p)
                <div class="row" style="border: 2px solid black; margin:5px; background-color:#B2B2CC;">
                    <div class="col-sm-2 col-xs-2">
                        <img src="{{{ $p->icon_url }}}" alt="no image" height="60" width="60" />
                    </div>
                    <div class="col-sm-2 col-xs-2">
                       <h4>{{{ $p->username }}}</h4>
                    </div>
                    
                    <div class="col-sm-8 col-xs-8" style="border-left:black 2px dotted; background-color:#F0F0F5;">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 style="width:200px">{{{ $p->title }}}</h4>                                
                            </div>
                            <div class="col-sm-6">
                                <div style="margin-top:10px;">{{{ $p->date_updated }}}</div>
                            </div>
                        </div>
                        
                        <div>{{{ $p->message }}}</div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-3" style="width:100px;">
                                <a href='{{{ url("view_post/$p->id") }}}' class="btn">View {{{ $p->count }}} comments</a>
                            </div>
                            <div class="col-sm-4 pull-right" style="width:100px;">
                                <a href='{{{ url("update_post/$p->id") }}}' class="btn">Update</a>
                            </div>
                            <div class="col-sm-3 pull-right" style="width:100px;">
                                <a href='{{{ url("delete_post_action/$p->id") }}}' class="btn">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
