@extends('layouts.master')

@section('title')
Social Network - Update Posts
@stop

@section('content')
    <div class="row">
        <div class="col-sm-4 col-xs-12">
            <form action="{{{ url('update_post_action') }}}" method="post">
                <div>
                    <h3>Name:</h3>
                    <span><h4>{{{ $post->username }}}</h4></span>
                </div>
                <div>
                    <h3>Title:</h3>
                    <span><input type="text" name="title" style="width:250px;" value="{{{ $post->title }}}" /></span>
                </div>
                <div>
                    <h3>Message:</h3>
                     <textarea name="message" style="width:250px; height:80px;">
                         {{{ $post->message }}}
                     </textarea> 
                </div>
                <input type="hidden" name="id" value="{{{ $post->id }}}"/>
                <a href="{{{ url('home') }}}" value="Cancel" class="offset2">Cancel</a>
                <input type="submit" value="Update" class="offset2">
            </form>
            
        </div>
    </div>
@stop
