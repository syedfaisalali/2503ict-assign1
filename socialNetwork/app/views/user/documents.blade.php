@extends('layouts.master')

@section('title')
Social Network - Posts
@stop

@section('content')
<style type="text/css">
    .box-style{
        border: 1px #8A8A8A solid;
        border-radius: 10px;
        padding: 10px;
        background-color:#E6E6E6;
        margin:10px
    }
    
    #table-of-content {
        padding: 10px;
        margin:10px
    }
    
    #up {
        position: fixed;
        color:black;
        float: right;
        border-radius:5px;
        border: 1px black solid;
        background-color: #94FFB8;
        padding:2px 0px 5px 10px;
        width: 160px;
        height: 40px;
        margin-left:79%;
        margin-top:40%;
    }
    
    .code-block {
        background-color:#A4AAB8;
        border: dotted 1px black;
        margin:10px;
    }
</style>
<div class="container">
    <div id="up">
        <h4><a href="#table-of-content" style="color:black">Table of content</a></h4>
    </div>
    <div id="table-of-content" class="row">
        <h2>TABLE OF CONTENTS</h2>
        <ol>
            <li>
                <a href="#site-diagram">Site Diagram</a>
            </li>
            <li>
                <a href="#db-design">Database Design</a>
            </li>
            <li>
                <a href="#laravel-report">Laravel Report</a>
            </li>
            <li>
                <a href="#site-report">Assignment documentation</a>
            </li>
        </ol>
    </div>
    <div id="site-diagram" class="row box-style">
        <h2>SITE DIAGRAM</h2>
        <img src="doc/site-diagram.jpg"></img>
    </div>
    <div class="row box-style" id="db-design">
        <h2>DATABASE DESIGN</h2>
        <img src="doc/posts.jpg"></img>
    </div>
    <div class="row box-style" id="laravel-report">
        <h2>LARAVEL REPORT</h2>
        <h4>Overview of changes</h4>
        A lot changes are made between the updation of Laravel from version 4.2 to 5.0. A brief of few changes are mentioned as follows:
        <ol>
            <li>A number of sdks have to be installed and upated for Laravel 5.0.</li>
            <li>.env.php has changed to .env in 5.0 (Config-Migration environment variables)</li>
            <li>In Laravel 5.0 “app/config/{environmentName}/” directories are no longer used to  configuration variables for different environments . Instead, it is required to set the configuration variables for different environments into .env. These variables can then be accessed using env('key', 'default value') in different configuration files.</li>
            <li>Route.php and controllers are to be placed in “app/Htpp/” folder rather than “app” folder.</li>
            <li>In laravel 5.0  CSRF protection is enabled by default on all routes. It can be enabled or disabled using a certain command across the application.</li>
            <li>Route filters configuration and placement has also been changed in laravel 5.0. The user is require to move their filter bindings from app/filters.php and place them in app/Providers/RouteServiceProvider.php file under the boot() method. Furthermore, in order to continue using Route Façade, user is required to add “use Illuminate\Support\Facades\Route”; in the app/Providers/RouteServiceProvider.php. All the filters such as auth and csrf are not required to be moved, they are already in the above mentioned file but as a middleware.</li>
            <li>A fair amount of changes are required to update artisan commands in the updated version.  All the old commands from “app/commands” folder are to be moved to new “app/Console/Commands” directory. Also the user is required to add this new folder to “classmap” directive of composer.json file. To finish off, the list of Artisan commands are to be moved from “start/artisan.php” into the command array of the “app/Console/Kernel.php”file.</li>
            <li>A little change is also required in database side of application to upgrade to newer version. Instead of storing migration classes in “app/database/migrations”, now store them in the new “database/migrations” directory. All the seeds should now be stored in “database/seeds” instead of “app/database/seeds”.</li>
            <li>Views are now stored in “resource/views” instead of “app/views” directory.</li>
            <li>For the sake of security reasons, Laravel 5.0 escapes output from both the { { } } and { { { } } } Blade directives by default. To display un-escaped and raw outputs a new directive {!! !!} has been introduced. It is recommended to use only the new directive do display raw output.</li>
            <li>Assets are to be placed in public folder in laravel 5.0 just as in version 4.2.</li>
            <li>The “Remote” and “Workbench” component is no longer used in the newer version.</li>
        </ol>
        <h4>Detail about changes in "Form and Html Helpers"</h4>
        In Laravel 5.0 the “Form” and “Html” helpers are deprecated therefore the users will receive error if they continue to use them in their code. These helpers are replaced by community-driven helpers. “Laravel Collecive” is one of the community maintaining such helpers.
        As an example of how a user can use the new helpers, he may add  "laravelcollective/html": "~5.0" in require section of the composer.json file. In order for this to work, he has to add the Form and HTML facades and service provider to his application. To do this the user must add the following lines to ‘providers’ array in “config/app.php” file:
        <br/><br/><span class="code-block">Collective\Html\HtmlServiceProvider</span>
        <br/><br/>Also, add these lines to the 'aliases' array:
        <br/><br/><span class="code-block">'Form' => 'Collective\Html\FormFacade'</span>
        <br/><br/><span class="code-block">'Html' => 'Collective\Html\HtmlFacade'</span>
    </div>
    <div class="row box-style" id="site-report">
        <h2>ASSIGNMENT DOCUMENTATION</h2>
        <h4>Tasks completed</h4>
        I was able to complete all the tasks required for the assingment, which included:
        <ol>
            <li>Creating a <b>master</b> layout that will be inherited in all the views</li>
            <li>Creating <b>home</b> page listing <b>all posts</b> and <b>add post form</b></li>
            <li>Handling <b>input validations</b></li>
            <li>Creating <b>view</b>,<b>delete</b> and <b>update</b> functionality for each post</li>
            <li>Creating <b>view posts</b> page with <b>add comments form</b> and <b>list comments</b></li>
            <li><b>Deleting</b> comments</li>
            <li>Appropriate <b>redirections</b></li>
        </ol>
        <h4>New approach</h4>
        <ol>
            <li>Rather then doing <b>CRUD operations</b> for each entity in routes, I handled that in each entities seperate <b>models</b></li>
            <li>I applied a <b>row count</b> for <b>number of comments</b> with in the query that fetches posts</li>
            <li>I tried to keep the UI as <b>flat</b> as possible for better user viewing.</li>
        </ol>
    </div>
</div>
@stop
