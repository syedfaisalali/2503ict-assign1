@extends('layouts.master')

@section('title')
Social Network - Comments
@stop

@section('content')
    <div class="row">
        <div class="col-sm-4 col-xs-12">
            <h2>Post comment</h2>
            <form action="{{{ url('add_comment_action') }}}" method="post">
                <div>
                    <h4>Name:</h4>
                    <span><input type="text" name="username" style="width:250px;" required/></span>
                </div>
                <div>
                    <h4>Message:</h4>
                     <textarea name="message" style="width:250px; height:80px;" required></textarea>
                </div>
                <input type="hidden" name="post_id" value="{{{ $post->id}}}"/>
                <input type="submit" value="Comment" class="offset2">
            </form>
            
        </div>
        
        <div class="col-sm-8 col-xs-12">
            <div class="row" style="border: 2px solid black; margin:5px; background-color:#B2B2CC;">
                    <div class="col-sm-2 col-xs-2">
                        <img src="../{{{ $post->icon_url }}}" alt="no image" height="60" width="60" />
                    </div>
                    <div class="col-sm-2 col-xs-2">
                       <h4>{{{ $post->username }}}</h4>
                    </div>
                    
                    <div class="col-sm-8 col-xs-8" style="border-left:black 2px dotted; background-color:#F0F0F5;">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 style="width:200px">{{{ $post->title }}}</h4>                                
                            </div>
                            <div class="col-sm-6">
                                <div style="margin-top:10px;">{{{ $post->date_updated }}}</div>
                            </div>
                        </div>
                        <div>{{{ $post->message }}}</div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-3">
                                <div>{{{ $post->count }}} comments</div>
                            </div>
                            <div class="col-sm-4 pull-right" style="width:100px;">
                                <a href='{{{ url("update_post/$post->id") }}}' class="btn">Update</a>
                            </div>
                            <div class="col-sm-3 pull-right" style="width:100px;">
                                <a href='{{{ url("delete_post_action/$post->id") }}}' class="btn">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            @foreach($comments as $c)
                <div class="row" style="border: 2px solid black; margin:5px; background-color:#B2B2CC;">
                    <div class="col-sm-2 col-xs-2">
                       <h4>{{{ $c->username }}}</h4>
                    </div>
                    
                    <div class="col-sm-8 col-xs-8" style="border-left:black 2px dotted; background-color:#F0F0F5;">
                        <div>{{{ $c->message }}}</div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-6">{{{ $c->date_posted }}}</div>
                            <div class="col-sm-3 pull-right" style="width:100px;">
                                <a href='{{{ url("delete_comment_action?id=$c->id&post_id=$post->id") }}}' class="btn">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop
