<?php

/* models containing CRUD for each entity */
require "models/posts.php";
require "models/comments.php";
require "models/friends.php";

// Default route for the application
Route::get('/', function()
{
    $posts = getPosts();
    return View::make('user.posts')->withPosts($posts);
	
});

// Same as default route but with meaningful name
Route::get('/home', function()
{
    $posts = getPosts();
    return View::make('user.posts')->withPosts($posts);
});

Route::get('/friends', function()
{
    $friends = getFriends();
    
    return View::make('user.friends')->withFriends($friends); 
});

Route::get('/documents', function()
{
    return View::make('user.documents');
});

Route::post('add_post_action', function()
{
     $username = Input::get('username');
     $title = Input::get('title');
     $message = Input::get('message');

    $success = addPost($username, $title, $message);

    if ($success) 
    {
        return Redirect::to(url("home"));
    } 
    else
    {
        die("Error adding item");
    }
});

Route::get('update_post/{id}', function($id)
{
    $post = getPost($id);
    return View::make('user.update_posts')->withPost($post[0]);
});

Route::post('update_post_action', function()
{
    $title = Input::get('title');
    $message = Input::get('message');
    $id = Input::get('id');

    $success = updatePost($id,  $title, $message);

    // If successfully created then display updated post
    if ($success) 
    {
        return Redirect::to(url("view_post/$id"));
    } 
    else
    {
        die("Error adding item");
    }
});

Route::get('view_post/{id}', function($id){
    $comments = getComments($id);
    $post = getPost($id);
    return View::make('user.view_post', array('post' => $post[0], 'comments' => $comments));
});

Route::get('delete_post_action/{id}', function($id)
{
    $success = deletePost($id);

    if ($success) 
    {
        return Redirect::to(url("home"));
    } 
    else
    {
        die("Error adding item");
    }
});

Route::post('add_comment_action', function()
{
    $username = Input::get('username');
    $message = Input::get('message');
    $post_id = Input::get('post_id');

    $success = addComment($post_id, $username, $message);

    // If successfully created then display post with new comments
    if ($success) 
    {
        return Redirect::to(url("view_post/$post_id"));
    } 
    else
    {
        die("Error adding item");
    }
});

Route::get('delete_comment_action', function()
{
    $id = Input::get('id');
    $post_id = Input::get('post_id');
    $success = deleteComment($id);

    if ($success) 
    {
        return Redirect::to(url("view_post/$post_id"));
    } 
    else
    {
        die("Error adding item");
    }
});