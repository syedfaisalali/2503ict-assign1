drop table if exists posts;
drop table if exists comments;

create table posts (
  id integer primary key autoincrement,
  icon_url varchar(40),
  username varchar(40) not null,
  date_posted date not null,
  date_updated date,
  title varchar(40),
  message varchar(40)
);

create table comments (
  id integer primary key autoincrement,
  post_id int not null REFERENCES posts(id),
  username varchar(40) not null,
  date_posted date not null,
  message varchar(40)
);

/* Column names changed to avoid SQLite reserved words. */

/*insert into pms(number, name, party, duration, state, start, finish) values (1,'Edmund Barton','Protectionist','2 years, 8 months, 24 days','New South Wales','1901-01-01','1903-09-24');*/
