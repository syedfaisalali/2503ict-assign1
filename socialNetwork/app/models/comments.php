<?php
// Get post relevant comments
function getComments($post_id)
{
  $sql = 'select * from comments where post_id = ? order by date_posted desc';
  $comments = DB::select($sql, array($post_id));
  return $comments;
}

// Add comment to a post
function addComment($post_id, $username, $message)
{
  $sql = 'insert into comments (post_id, username, message, date_posted) values (?, ?, ?, ?)';
  /* 
    date(DATE_RFC2822) gives the latest date
  */
  $id = DB::insert($sql,array($post_id, $username, $message, date(DATE_RFC2822)));
  
  if($id)
  {
    return true;
  }
  else
  {
    return false;
  }
}

// Delete comment from a post
function deleteComment($id)
{
  $sql = "delete from comments where id = ?";
  
  DB::delete($sql, array($id));
  
  return true;
}
?>