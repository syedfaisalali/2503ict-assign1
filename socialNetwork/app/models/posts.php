<?php
// Get all posts from db
function getPosts()
{
  /*  
    sql query will select all posts along with 
    the count of its relevant commentswith recent
    post first
  */
  $sql = 'select p.*, COUNT(c.post_id) AS count FROM posts AS p outer left join comments AS c ON p.id=c.post_id GROUP BY p.id ORDER BY p.date_updated DESC;';
  $posts = DB::select($sql);
  return $posts;
}

function getPost($id)
{
  /*  
    sql query will select a specific post 
    along with its relevant comments count,
    with recent comment first
  */
  $sql = 'select p.*, COUNT(c.post_id) AS count FROM posts AS p outer left join comments AS c ON p.id=c.post_id WHERE p.id = ? GROUP BY p.id ';
  $post = DB::select($sql,array($id));
  return $post;
}

function addPost($username, $title, $message) 
{
    /*
      sql query will add new post to db.
      date(DATE_RFC2822) gets the recent date.
      'imb/smily_face.png' is the path to image
      for user.
    */
    $sql = "insert into posts (icon_url, username, title, message, date_posted, date_updated) values (?, ?, ?, ?, ?, ?)";
    
    DB::insert($sql, array('img/smily_face.png', $username, $title, $message, date(DATE_RFC2822), date(DATE_RFC2822)));
    
    // Get the id of the last added item.
    $id = DB::getPdo()->lastInsertId();
    
    if($id){
        return true;
    } else {
        return false;
    }
}

function updatePost($id, $title, $message) 
{
  $sql = "update posts set title = ?, message = ?, date_updated = ? where id = ?";
  
  DB::update($sql, array($title, $message, date(DATE_RFC2822), $id));
  
  return $id;
}

function deletePost($id) 
{
  $sql = "delete from posts where id = ?";
  
  // Delete post from posts table
  DB::delete($sql, array($id));
  
  // Delete post relevant comments from comments table.
  DB::delete('delete from comments where post_id = ?', array($id));
  
  return true;
}
?>